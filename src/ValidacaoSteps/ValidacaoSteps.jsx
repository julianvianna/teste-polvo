import React, { Component } from 'react';
import './ValidacaoSteps.css';

class ValidacaoSteps extends Component {

	constructor(props) {
		super(props);
	}

	stepBox = () => {

		if (this.props.step == 1) {
			return (
				<div className="stepsBar lowbar row">
					<div className="col"><span></span></div>
					<div className="col"><span></span></div>
					<div className="col"><span></span></div>
				</div>
			)
		}

		if (this.props.step == 2) {
			return (
				<div className="stepsBar mediumbar row">
					<div className="col"><span></span></div>
					<div className="col"><span></span></div>
					<div className="col"><span></span></div>
				</div>
			)
		}

		if (this.props.step == 3) {
			return (
				<div className="stepsBar fullbar row">
					<div className="col"><span></span></div>
					<div className="col"><span></span></div>
					<div className="col"><span></span></div>
				</div>
			)
		}

		else {
			return (
				<div className="stepsBar bar row">
					<div className="col"><span></span></div>
					<div className="col"><span></span></div>
					<div className="col"><span></span></div>
				</div>
			)
		}
	}

	render() {
		return (
			<div className="container">
				{this.stepBox()}
			</div>
		)
	}
}

export default ValidacaoSteps;