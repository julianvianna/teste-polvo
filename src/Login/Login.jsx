import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { Card, CardBody, CardTitle } from 'reactstrap';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import ValidacaoSteps from '../ValidacaoSteps/ValidacaoSteps';
import './Login.css';

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			senha: '',
			valNum: false,
			valCaps: false,
			valTam: false,
			valSteps: 0
		}
	}

	handlerSenha = (evt) => {

		let senhaVal = evt.target.value;
		let regExNum = new RegExp("[0-9]");
		let regExCaps = new RegExp("[A-Z]");
		let valSteps = 0;

		if (this.state.valTam) {
			if (this.state.valStep)
				this.setState({ valStep: this.state.valStep + 1 });
		}
		else {
			if (this.state.valStep > 0) {
				this.setState({ valStep: this.state.valStep - 1 });
			}
		}

		// VALIDA SENHA
		if (regExNum.test(senhaVal)) {
			this.setState({ valNum: true });
		}
		else {
			this.setState({ valNum: false });
		}

		// VALIDA CAPS
		if (regExCaps.test(senhaVal)) {
			this.setState({ valCaps: true });
		}
		else {
			this.setState({ valCaps: false });
		}

		// VALIDA QTDE DE CHARS
		if (senhaVal.length > 5) {
			this.setState({ valTam: true });
		}
		else {
			this.setState({ valTam: false });
		}

		this.setState({ senha: evt.target.value });

		this.state.valNum ? valSteps++ : valSteps;
		this.state.valCaps ? valSteps++ : valSteps;
		this.state.valTam ? valSteps++ : valSteps;

		this.setState({ valSteps: valSteps });
	}

	render() {
		return (
			<Container>
				<Row>
					<Col sm={4}></Col>
					<Col sm={4} className="formCadastro">
						<Card>
							<CardTitle className="logo">
								<img src="/img/logo_polvo_colorido.svg" alt="Polvo" />
							</CardTitle>
							<CardBody>

								<Form>
									<FormGroup>
										<Label for="email">Email</Label>
										<Input type="email" name="email" id="email" placeholder="e-mail" />
									</FormGroup>
									<FormGroup>
										<Label for="senha">Senha</Label>
										<Input type="password" className={"inputVal step" + this.state.valSteps} onKeyUp={this.handlerSenha} />
									</FormGroup>

									<ValidacaoSteps step={this.state.valSteps} />

									<div className="listValidacao">
										<ul>
											<li className={this.state.valTam ? 'active' : ''}><span>&bull;</span> Pelo menos 6 caracteres</li>
											<li className={this.state.valCaps ? 'active' : ''}><span>&bull;</span> Pelo menos 1 letra maiúscula</li>
											<li className={this.state.valNum ? 'active' : ''}><span>&bull;</span> Pelo menos 1 número</li>
										</ul>
									</div>

									<Button className="enviarBt">Cadastrar</Button>
								</Form>

							</CardBody>
						</Card>
					</Col>
					<Col sm={4}></Col>
				</Row>
			</Container >
		);
	}
}

export default Login;
